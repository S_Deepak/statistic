using testrobot as my from '../db/data-model';
@odata.draft.enabled: true
service CatalogService {
  entity Statistic@insert  as projection on my.Statistic ;
    
  }


annotate CatalogService.Statistic with @(
    UI: {
      HeaderInfo: {
        TypeName: '{i18n>Cat.TypeName}',
        TypeNamePlural: '{i18n>Cat.TypeNamePlural}',
        Title: { $Type: 'UI.DataField', Value: 'TEST'}
      },    
      SelectionFields: [ result, date],      
      LineItem: [
        {$Type: 'UI.DataField', Value: ID},
        {$Type: 'UI.DataField', Value: date},
        {$Type: 'UI.DataField', Value: time},
        {$Type: 'UI.DataField', Value: value1},
        {$Type: 'UI.DataField', Value: value2},
        {$Type: 'UI.DataField', Value: result},
      ],
      HeaderFacets: [       
        {$Type: 'UI.ReferenceFacet', Target: '@UI.FieldGroup#StatisticsDetail', Label:'{i18n>Cat.HeaderFacetDetails}' },
        
      ],
      Facets: [
        {
          $Type: 'UI.CollectionFacet',
          Label: '{i18n>Cat.FacetStatisticsInformation}',
          Facets: [
            {$Type: 'UI.ReferenceFacet', Target: '@UI.FieldGroup#Description', Label: '{i18n>Cat.FacetSectionDescription}'},
          ] 
        }
      ],
      QuickCreateFacets:[
        {
          $Type: 'UI.CollectionFacet',
          LineItem: [
            {$Type: 'UI.DataField', Value: ID},
            {$Type: 'UI.DataField', Value: date},
            {$Type: 'UI.DataField', Value: time},
            {$Type: 'UI.DataField', Value: value1},
            {$Type: 'UI.DataField', Value: value2},
            {$Type: 'UI.DataField', Value: result},
          ],


        }

      ],
     

      FieldGroup#Description: {
        Data:[
            {$Type: 'UI.DataField', Value: description}
        ]
      },          
      FieldGroup#StatisticsDetail: {
        Data:[
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: date},
          {$Type: 'UI.DataField', Value: time},
          {$Type: 'UI.DataField', Value: result},
        ]
      }
    }
);

annotate CatalogService.Statistic with {
  ID @( Common: { Label: '{i18n>Cat.StatisticsID}'} );
  description @( Common.Label: '{i18n>Cat.Statisticsdescription}' );
  date @( Common.Label: '{i18n>Cat.Statisticsdate}' );
  time @( Common.Label: '{i18n>Cat.Statisticstime}' );
  value1 @( Common.Label: '{i18n>Cat.Statisticsvalue1}' );
  value2 @( Common.Label: '{i18n>Cat.Statisticsvalue2}' );
  result @( Common.Label: '{i18n>Cat.Statisticsresult}' );


}

