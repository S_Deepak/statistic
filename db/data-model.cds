namespace testrobot;
using { Country, managed } from '@sap/cds/common';

  entity Statistic {
    key ID : Integer;
    description   : String;
    date   : Date;
    time   : Time;
    value1 : Integer;
    value2 : Integer;
    result : String
  }

  